package com.alexeteg.inboxtest.manager

import org.openqa.selenium.By
import org.openqa.selenium.Keys
import com.alexeteg.inboxtest.LoggableDriver
import java.io.File

class TestManagerImpl(private val driver: LoggableDriver) : TestManager {

    private val loginUrl = "https://e.tusur.ru/account/login"

    override fun openPage() {
        driver.apply {
            get(loginUrl)
            until { it.currentUrl == loginUrl }
        }
    }

    override fun auth(login: String, password: String) {
        driver.apply {
            assertLog(driver.currentUrl == loginUrl) {
                "Должна быть открыта страница входа"
            }
            findElement(By.id("username")).let {
                it.sendKeys(login)
                assertLog(it.getAttribute("value") == login) {
                    "Логин не установлен"
                }
            }
            findElement(By.id("password")).sendKeys(password)
            findElement(By.className("btn-success")).click()

            runCatching {
                findElement(By.className("alert-success"))
            }.onSuccess { alertSuccessElement ->
                assertLog(alertSuccessElement.isDisplayed) {
                    "Неправильная почта или пароль"
                }
                val inboxUrl = "https://e.tusur.ru/webmail"
                get(inboxUrl)
                until { it.currentUrl == inboxUrl }
            }.onFailure {
                assertLog(it !is NoSuchElementException) {
                    "Неправильная почта или пароль"
                }
            }
        }
    }

    override fun openNewLetter() {
        driver.apply {
            val sliderBarElement = findElement(By.className("sidebar"))
            val buttonElement = sliderBarElement.findElement(By.className("btn-default"))
            buttonElement.click()
            val sendUrl = "https://e.tusur.ru/webmail/send"
            get(sendUrl)
            until { it.currentUrl == sendUrl }
        }
    }

    override fun setTitle(title: String) {
        driver.apply {
            findElement(By.id("inputSubject")).let {
                it.sendKeys(title)
                assertLog(it.getAttribute("value") == title) { "Тема письма не установлена" }
            }
        }
    }

    override fun setReceiver(receiver: String) {
        driver.apply {
            findElement(By.id("inputTo")).let {
                it.sendKeys(receiver)
                assertLog(it.getAttribute("value") == receiver) { "Получатель письма не установлен" }
            }
        }
    }

    override fun sendLetter() {
        driver.apply {
            /* Click SEND MESSAGE */
            val formGroupElement = findElement(By.className("form-group"))
            formGroupElement.findElement(By.className("btn-primary")).click()

            /* Click YES, SEND */
            val sendModelElement = findElement(By.id("sendModal"))
            sendModelElement.findElement(By.className("bulk-send-confirm")).click()

            until {
                findElement(By.className("alert-success")).isDisplayed
            }
        }
    }

    override fun setContent(from: String) {
        driver.apply {
            setFile(File("src/test/resources/image.jpg"))
            findElement(By.className("note-editable")).run {
                sendKeys(File("src/test/resources/description.txt").readText())
                sendKeys(Keys.RETURN)
                sendKeys(Keys.chord(Keys.CONTROL, "k"))

                val sendFormModalElement = driver.findElement(By.className("modal-content"))
                val modalBodyElement = sendFormModalElement.findElement(By.className("modal-body"))
                modalBodyElement.findElement(By.className("note-link-url"))
                    .sendKeys("https://e.tusur.ru/webmail")
                sendFormModalElement.findElement(By.className("note-link-btn")).click()

                sendKeys(Keys.ENTER)
                sendKeys(from)
            }
        }

    }

    private fun setFile(file: File) {
        driver.apply {
            val inputElement = findElement(By.id("input-attachment"))
            inputElement.sendKeys(file.absolutePath)
            val isAttachFile = !inputElement.text.contains("Файл не выбран")
            assertLog(isAttachFile) {
                "Файл не установлен"
            }
            if (isAttachFile) {
                until {
                    inputElement.isDisplayed
                }
            }
        }
    }
}