package com.alexeteg.inboxtest

import org.junit.After
import org.junit.Test
import org.junit.Before
import org.openqa.selenium.chrome.ChromeDriver
import org.openqa.selenium.support.PageFactory
import com.alexeteg.inboxtest.manager.TestManager
import com.alexeteg.inboxtest.manager.TestManagerImpl
import java.util.concurrent.TimeUnit

class TusurMailTest {
    private var driver = LoggableDriver(ChromeDriver(), TIMEOUT)
    private val testManger: TestManager = TestManagerImpl(driver)

    @Before
    fun initDriver() {
        PageFactory.initElements(driver, this)
        driver.manage()?.timeouts()?.implicitlyWait(TIMEOUT, TimeUnit.SECONDS)
        driver.manage()?.window()?.maximize()
    }

    @Test
    fun startTest() {
        val login = System.getProperty("login")
        val password = System.getProperty("password")
        val title = System.getProperty("title")
        val receiver = System.getProperty("receiver")
        val from = System.getProperty("from")
        with(testManger) {
            openPage()
            auth(login, password)
            openNewLetter()
            setTitle(title)
            setReceiver(receiver)
            setContent(from)
            sendLetter()
        }
    }

    @After
    fun endTest() {
        driver.close()
    }

    private companion object {
        const val TIMEOUT = 10L
    }
}